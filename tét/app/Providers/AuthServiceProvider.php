<?php

namespace App\Providers;

use App\Model\User;
use App\Model\Admin;
use App\Model\Product;
use App\Model\Category;
use App\Model\Tag;
use App\Policies\AdminPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\ProductPolicy;
use App\Policies\TagPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Product::class => ProductPolicy::class,
        Admin::class =>AdminPolicy::class,
        Tag::class => TagPolicy::class,
        Category::class => CategoryPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Gate::define('bill', function () {
        //     return Auth::check() && Auth::user()->isHaveItemsInCurrentCart();
        // });
    }
}
